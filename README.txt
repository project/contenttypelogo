-- SUMMARY --

* The Content Type Logo module allows admin users to provide a logo specific 
path for each content type.
* Using this module we can add image style to an logo, we can add 
logo link title and logo image alt text.


-- REQUIREMENTS --

Site should have Site Branding Block enabled and assigned to a region

-- CONFIGURATION --

* The module provide a configuration form in Site Branding Block where we can 
provide path for logo
* Configure user permissions in Administration » Structure » Block layout:   

-- CUSTOMIZATION --

* In this module we have overwritten the template
(block--system-branding-block.html.twig) of Site Branding Block. If you theme 
has already overwritten this template make sure, in you template add the 
variables used in this module template.

-- CONTACT --

Current maintainers:
* Sreenivasulu Paruchuri(sreenivasparuchuri)-https://www.drupal.org/user/2881691
